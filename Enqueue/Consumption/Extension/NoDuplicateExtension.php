<?php

namespace Garlic\Bus\Enqueue\Consumption\Extension;

use Enqueue\Consumption\Context;
use Enqueue\Consumption\EmptyExtensionTrait;
use Enqueue\Consumption\ExtensionInterface;
use Interop\Queue\PsrProcessor;
use Predis\Client;

class NoDuplicateExtension implements ExtensionInterface
{
    const SUCCESS_TTL = 3600;
    const REDIS_SUCCESS_KEY = 'success.messages.%s';
    const PROCESS_TTL = 600;
    const REDIS_PROCESS_KEY = 'process.messages.%s';

    use EmptyExtensionTrait;

    /**
     * @var Client
     */
    private $redis;

    /**
     * NoDuplicateExtension constructor.
     *
     * @param Client $redis
     */
    public function __construct(Client $redis)
    {
        $this->redis = $redis;
    }

    public function onPreReceived(Context $context)
    {
        $messageHash = hash('MD5', $context->getPsrMessage()->getBody());
        if (!empty($this->redis->get(sprintf(self::REDIS_SUCCESS_KEY, $messageHash)))) {
            $context->setResult(PsrProcessor::ACK);

            return;
        }

        if (!empty($this->redis->get(sprintf(self::REDIS_PROCESS_KEY, $messageHash)))) {
            sleep(10);
            $context->setResult(PsrProcessor::REQUEUE);

            return;
        }

        $this->redis->setex(sprintf(self::REDIS_PROCESS_KEY, $messageHash), self::PROCESS_TTL, $messageHash);
    }

    /**
     * {@inheritdoc}
     */
    public function onPostReceived(Context $context)
    {
        if (PsrProcessor::ACK === $context->getResult()) {
            $messageHash = hash('MD5', $context->getPsrMessage()->getBody());
            $this->redis->setex(sprintf(self::REDIS_SUCCESS_KEY, $messageHash), self::SUCCESS_TTL, $messageHash);
        }
    }
}
